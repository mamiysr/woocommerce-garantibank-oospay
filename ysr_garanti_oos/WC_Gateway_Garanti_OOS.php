<?php 

/**
* 
*/
class WC_Gateway_Garanti_OOS extends WC_Payment_Gateway {

	public $Mode = "PROD";
	public $ApiVersion = "v0.01";
	public $TerminalProvUserID = "PROVOOS";
	public $Type = "sales";
	public $CurrencyCode = "949";
	public $Lang = "tr";
	public $liveurl = 'https://sanalposprov.garanti.com.tr/servlet/gt3dengine';
	public $testurl = 'https://sanalposprovtest.garanti.com.tr/servlet/gt3dengine';
	public $secure3dsecuritylevel = 'OOS_PAY'; 
	public $Installmentcount = '';

	function __construct(){
		$this->id 					= "yorci_garanti_oos";
		$this->method_title 		= __( "Garanti Bank OOSPay", 'yorci_garanti_oos' );
		$this->method_description 	= __( "WooCommerce için Garanti Bankası Ortak Ödeme Sayfası", 'yorci_garanti_oos' );
		$this->title 				= __( "Garanti OOSPay", 'yorci_garanti_oos' );
		$this->notify_url 			= WC()->api_request_url( 'WC_Gateway_Garanti_OOS' );

		$this->icon = NULL;
		$this->has_fields = true;
		$this->init_form_fields();
		$this->init_settings();

		$this->firma_adi 		= $this->get_option('firma_adi');
		$this->terminalID 		= $this->get_option('terminalID');
		$this->merchantID 		= $this->get_option('merchantID');
		$this->storeKey 		= $this->get_option('storeKey');
		$this->provisionPass 	= $this->get_option('provisionPass');
		$this->description 		= $this->get_option( 'description' );
		$this->test_ortami 		= $this->get_option('test_ortami');

		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action(	'woocommerce_receipt_yorci_garanti_oos', array( $this, 'receipt_page' ));
		add_action( 'GarantibankCallback', array( $this, 'gelen_veri' ) );
		add_action( 'woocommerce_api_wc_gateway_garanti_oos', array( $this, 'handle_callback' ));
	}
	

	public function init_form_fields() {
		$this->form_fields = array(
			'enabled' => array(
				'title'   => __( 'Enable/Disable', 'woocommerce' ),
				'type'    => 'checkbox',
				'label'   => __( 'Garanti OOSPay Etkin', 'woocommerce' ),
				'default' => 'yes'
				),
			'test_ortami' => array(
				'title' => __( 'Test Modu?', 'yorci_garanti_oos' ),
				'type' => 'checkbox',
				'label' => __( 'Test Modu Açık mı?', 'yorci_garanti_oos' ),
				'default' => 'yes'
				),
			'description' => array(
				'title'       => __( 'Description', 'woocommerce' ),
				'type'        => 'text',
				'desc_tip'    => true,
				'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce' ),
				'default'     => __( 'Garanti Ortak Ödeme Sayfası ile güvenli ödeme sunar.', 'woocommerce' )
				),
			'firma_adi' => array(
				'title' => __( 'Firma Adi', 'yorci_garanti_oos' ),
				'type' => 'text',
				'description' => __( 'Faturada Görünecek Firma Adı.', 'yorci_garanti_oos' ),
				'default' => __( 'YSR Garanti OOS Test', 'yorci_garanti_oos' ),
				'desc_tip'      => true,
				),
			'terminalID' => array(
				'title' => __( 'Garanti Terminal ID', 'yorci_garanti_oos' ),
				'type' => 'text',
				'description' => __( 'Bankanın Size Sağladığı 8 Karakterli Terminal ID.', 'yorci_garanti_oos' ),
				'default' => __( '30691300', 'yorci_garanti_oos' ),
				'desc_tip'      => true,
				),
			'merchantID' => array(
				'title' => __( 'Garanti Merchant ID', 'yorci_garanti_oos' ),
				'type' => 'text',
				'description' => __( 'Bankanın Size Sağladığı 7 Karakterli Merchant ID.', 'yorci_garanti_oos' ),
				'default' => __( '7000679', 'yorci_garanti_oos' ),
				'desc_tip'      => true,
				),
			'storeKey' => array(
				'title' => __( 'Garanti storeKey', 'yorci_garanti_oos' ),
				'type' => 'text',
				'description' => __( 'Bankanın Size Sağladığı storeKey.', 'yorci_garanti_oos' ),
				'default' => __( '12345678', 'yorci_garanti_oos' ),
				'desc_tip'      => true,
				),
			'provisionPass' => array(
				'title' => __( 'Garanti ProvisionPassword', 'yorci_garanti_oos' ),
				'type' => 'text',
				'description' => __( 'Bankanın Size Sağladığı ProvisionPassword.', 'yorci_garanti_oos' ),
				'default' => __( '123qweASD', 'yorci_garanti_oos' ),
				'desc_tip'      => true,
				),
			);
	}

	public function receipt_page($order){
		echo '<div class="alert alert-info" role="alert">'.__('Siparişiniz için teşekkür ederiz, Garanti OOSPay ile ödeme için aşağıdaki butona tıklayınız. Bu sizi bilgilerinizi güvenli şekilde girmeniz için bankaya yönlendirecektir, ödeme işleminden hemen sonra tekrar sitemize döneceksiniz.', 'yorci_garanti_oos').'</div>';
		echo $this->veri_getir($order);
	}

	public function veri_getir( $order_id ) {
		global $woocommerce;
		$mus_siparis = new WC_Order($order_id);

		$terminalID_ = '0'.$this->terminalID;

		$order_total = str_replace(array('.', ','), '' , $mus_siparis->order_total);
		$pos_order_id = 'SiparisNo'.$mus_siparis->id;
		$datetime_obj = new DateTime();
		$timestamp = $datetime_obj->getTimestamp();

		$SecurityData = strtoupper(sha1($this->provisionPass.$terminalID_));
		$HashData = strtoupper(sha1($this->terminalID.$pos_order_id.$order_total.$this->notify_url.$this->notify_url.$this->Type.$this->Installmentcount.$this->storeKey.$SecurityData));
		
		$postparams = array(
			'secure3dsecuritylevel' => $this->secure3dsecuritylevel,
			'refreshtime' => '5',
			'mode' => $this->Mode,
			'apiversion' => $this->ApiVersion,
			'terminalprovuserid' => $this->TerminalProvUserID,
			'terminaluserid' => $this->TerminalProvUserID,
			'terminalid' => $this->terminalID,
			'terminalmerchantid' => $this->merchantID,
			'orderid' => $pos_order_id,
			'customeremailaddress' => $mus_siparis->billing_email,
			'customeripaddress' => $_SERVER['REMOTE_ADDR'],
			'txntype' => $this->Type,
			'txnamount' => $order_total,
			'txncurrencycode' => get_woocommerce_currency() == "TRY" ? $this->CurrencyCode : '',
			'companyname' => $this->firma_adi,
			'txninstallmentcount' => $this->Installmentcount,
			'successurl' => $this->notify_url,
			'errorurl' => $this->notify_url,
			'secure3dhash' => $HashData,
			'lang' => $this->Lang,
			'txntimestamp' => $timestamp,
			'orderaddresscount' => 1,
			'orderaddresscity1' => $mus_siparis->billing_city,
			'orderaddresscompany1' => $mus_siparis->billing_company,
			'orderaddresscountry1' => $mus_siparis->billing_country,
			'orderaddressphonenumber1' => $mus_siparis->billing_phone,
			'orderaddressname1' => $mus_siparis->billing_first_name.' '.$mus_siparis->billing_last_name,
			'orderaddresspostalcode1' => $mus_siparis->billing_postcode,
			'orderaddresstext1' => $mus_siparis->billing_address_1.' '.$mus_siparis->billing_address_2.' '.$mus_siparis->billing_city.' / '.$mus_siparis->billing_state.' / '.$mus_siparis->billing_country,
			'orderaddresstype1' => 'B',
			);

		$garanti_args = array();
		foreach($postparams as $key => $value){
			$garanti_args[] = "<input type='hidden' name='".$key."' value='".$value."'/>";
		}
		if($this->test_ortami == 'yes'){
			$processURI = $this->testurl;
		}else{
			$processURI = $this->liveurl;
		}
		$html_form    =
		'<form action="'.$processURI.'" method="post" id="garanti_oos_payment_form">' 
		.implode('', $garanti_args) 
		.'<input type="submit" class="btn btn-boga" id="submit_garanti_oos_payment_form" value="'.__('Garanti OOSPay ile güvenli öde', 'yorci_garanti_oos').'" /> <a class="btn btn-danger cancel" href="'.$mus_siparis->get_cancel_order_url().'">'.__('Vazgeç ve sepeti geri getir', 'yorci_garanti_oos').'</a>'
		.'</form>';
		return $html_form;
	}

	public function handle_callback(){
		@ob_clean();
		if($_POST){
			$callback = stripslashes_deep($_POST);
			do_action( "GarantibankCallback", $callback );
		}else{
			wp_die( "Banka'dan veri alınamadı. Lütfen site yöneticileriyle iletişime geçin.", "Garanti Callback", array( 'response' => 200 ) );
		}
	}

	public function gelen_veri($post)
	{
		global $woocommerce;
		if($post['procreturncode'] == 00 && $post['mdstatus'] == 1)
		{
			$order_id = str_replace('SiparisNo','', $post['oid']);
			//sipariş bilgileri
			$order = wc_get_order($order_id);
			//sipariş tutarı
			$order_total = str_replace(array('.', ','), '' , $order->order_total);
			//Eğer sipariş tutarıyla bankanın çektiği ücret aynıysa.
			if($order_total == $post['txnamount']){
				$order->update_status('on-hold', __( 'Garanti Bank OOS ödemesi başarılı,', 'yorci_garanti_oos' ));
				$woocommerce->cart->empty_cart();
				$order->payment_complete();
				wp_redirect($this->get_return_url($order));
				exit;
			}else{
				wp_die('Ödenen tutar sipariş tutarına eşit değil!','HATA!',array('response' => 200));
			}
		}else{
			$errmsg = $post['errmsg'];
			$mderrmsg = $post['mderrormessage'];
			$hostmsg = $post['hostmsg'];
			$geri = '<a href="'.$woocommerce->cart->get_checkout_url().'" role="button">« Sipariş sayfasına geri dön</a>';
			wp_die($errmsg.'<br>'.$hostmsg.'<br>'.$geri,'HATA!',array('response' => 200));
		}

	}

	public function process_payment( $order_id ) {
		$order = wc_get_order($order_id);
		
		return array(
			'result'  => 'success',
			'redirect'  => $order->get_checkout_payment_url( true )
			);
	}

}