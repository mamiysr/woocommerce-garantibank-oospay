<?php
/*
Plugin Name: Garanti OOSPay - WooCommerce Gateway
Plugin URI: http://mamiysr.wordpress.com/
Description: Garanti Bankası Ortak Ödeme Sayfası WooCommerce Eklentisi.
Version: 1.1
Author: mamiysr (Işbara Han)
Author URI: http://yorci.com/
*/
 

add_action( 'plugins_loaded', 'init_garanti_oos_gateway', 0 );
function init_garanti_oos_gateway() {
   
    if ( ! class_exists( 'WC_Payment_Gateway' ) ) return;
     
    include_once( 'WC_Gateway_Garanti_OOS.php' );
 
    function add_garanti_oos_gateway( $methods ) {
        $methods[] = 'wc_gateway_garanti_oos';
        return $methods;
    }
    add_filter( 'woocommerce_payment_gateways', 'add_garanti_oos_gateway' );
}
 
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'ysr_garanti_oos_action_links' );
function ysr_garanti_oos_action_links( $links ) {
    $plugin_links = array(
        '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout' ) . '">' . __( 'Ayarlar', 'ysr_garanti_oos' ) . '</a>',
    );
     return array_merge( $plugin_links, $links );    
}